import 'package:flutter/material.dart';
import 'package:transparent_image/transparent_image.dart';

class MainPage extends StatefulWidget {
  const MainPage({super.key});

  @override
  State<MainPage> createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  String _currentUrl = "";

  @override
  Widget build(BuildContext context) {
    return SizedBox.expand(
      child: Container(
        padding: const EdgeInsets.all(20),
        child: Column(
          children: [
            Expanded(
              child: Wrap(
                spacing: 20,
                direction: Axis.vertical,
                runAlignment: WrapAlignment.center,
                children: [
                  Text('Image from url: $_currentUrl'),
                  FadeInImage.memoryNetwork(
                    placeholder: kTransparentImage,
                    image: _currentUrl,
                  ),
                ]
              )
            ),
            Row(
              children: [
                Expanded(
                  child: TextField(
                    onChanged: (String text) {
                      _currentUrl = text;
                    }
                  )
                ),
                ElevatedButton(
                  onPressed: () {
                    setState(() {});
                  },
                  child: Text('Show Image')
                )
              ]
            )
          ]
        )
      )
    );
  }
}
