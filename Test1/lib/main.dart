import 'package:flutter/material.dart';
import 'package:test_1/main_page.dart';

void main() => runApp(
  MaterialApp(
    title: 'Image View',
    theme: ThemeData(
      useMaterial3: true,
      colorScheme: ColorScheme.fromSeed(seedColor: Colors.red),
    ),
    home: Scaffold(
      appBar: AppBar(
        title: Text('Image View App'),
      ),
      body: MainPage(),
    )
  )
);
